public class Calculator
{
public static void main(String[] args)
{
	Calculator cc = new Calculator();
	float fl = cc.add();
	cc.divide("hii");
	cc.divide(fl); //parameter //method calling statement
	cc.divide(fl,5.5f);
	//Method overloading : compileTime polymorphism
	//Same method name with different no.of arguments or
	//with different type of argurment.
}
public float add(){ //return type required
	return 100+200.5f; //300.5f
}
public void divide(String msg){
	System.out.println(msg);
}
public void divide(float re){
	System.out.println(re/10);
}
public void divide(float re,float no)//argument
{ //method definition
	System.out.println(re/no);  //300.5/5
}
}